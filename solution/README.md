API REST letsgoi
=======
- Developer: Andrés Franco
- A Symfony project created on June 9, 2018, 11:35 pm.

Nota: Indicaciones para endpoints API-REST de pedidos/LETSGOI

### Método para que un DRIVER obtenga el listado de tareas para completar en el día ###

* http://127.0.0.1:8000/api/tarea/pendientes/1/2018-06-12

```json
[
    {
        "id": 1,
        "fecha_entrega": "2018-06-12T00:00:00+02:00",
        "franja_hora": 2,
        "domicilio": {
            "direccion": "Madrid.\r\nCalle Castelló 115, 5C\r\nCP 28543 ",
            "cliente": {
                "nombre": "Andrés",
                "apellidos": "Franco Cárdenas",
                "email": "andres.afranco@mail.com"
            }
        }
    },
    {
        "id": 4,
        "fecha_entrega": "2018-06-12T20:56:41+02:00",
        "franja_hora": 2,
        "domicilio": {
            "direccion": "Av. Constitución 98, 2D",
            "cliente": {
                "nombre": "Laura",
                "apellidos": "Candamir Gomez",
                "email": "laura.gomez@mail.com"
            }
        }
    }
]
```

### Método para crear pedido ###

* http://127.0.0.1:8000/api/pedido/guardar
- Ej:

```json
{
  "cliente":
	{
		"nombre": "Laura",
		"apellidos": "Candamir Gomez",
		"email": "laura.gomez@mail.com"
	},
  "id_domicilio": "",
  "direccion": "Av. Noriega 12, Bajo 3",
  "fecha_entrega": "2018-06-13",
  "franja_hora": 2
}
```


En el directorio 'docs' también se encuentran algunos ejemplos de los métodos