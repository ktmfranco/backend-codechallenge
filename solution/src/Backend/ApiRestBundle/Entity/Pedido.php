<?php

namespace Backend\ApiRestBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Groups;

use Doctrine\ORM\Mapping as ORM;

/**
 * Pedido
 *
 * @ORM\Table(name="Pedido")
 * @ORM\Entity(repositoryClass="Backend\ApiRestBundle\Repository\PedidoRepository")
 *
 * @ExclusionPolicy("none")
 */
class Pedido
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @Groups({"tareas_pendientes"})
     */
    private $id;


    /**
     * @var \DateTime
     *
     * @Assert\NotBlank(message = "Fecha de entrega es requerida")
     * @Assert\Date()
     * @ORM\Column(name="fecha_entrega", type="datetime", nullable=false)
     *
     * @Groups({"tareas_pendientes"})
     */
    private $fechaEntrega;

    /**
     * @var int
     *
     * @Assert\NotBlank(message = "Franja hora es requerida")
     * @Assert\Range(
     *      min = 1,
     *      max = 8,
     *      minMessage = "Franja de hora debe estar entre 1h a 8h",
     *      maxMessage = "Franja de hora debe estar entre 1h a 8h"
     * )
     * @ORM\Column(name="franja_hora", type="integer", nullable=false)
     *
     * @Groups({"tareas_pendientes"})
     */
    private $franjaHora;

    /**
     * @var \Backend\ApiRestBundle\Entity\Domicilio
     * @Assert\NotBlank(message = "Dirección es requerida")
     *
     * @ORM\ManyToOne(targetEntity="Backend\ApiRestBundle\Entity\Domicilio")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="domicilio", referencedColumnName="id", nullable=false)
     * })
     *
     * @Groups({"tareas_pendientes"})
     */
    private $domicilio;

    /**
     * @var \Backend\ApiRestBundle\Entity\Driver
     *
     * @ORM\ManyToOne(targetEntity="Backend\ApiRestBundle\Entity\Driver")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="driver", referencedColumnName="id")
     * })
     *
     * @Groups({"list_pedido"})
     */
    private $driver;

    /**
     * @var bool
     *
     * @ORM\Column(name="hecho", type="boolean")
     *
     * @Groups({"list_pedido"})
     */
    private $hecho;

    public function __construct(){
        $this->hecho = false;
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fechaEntrega
     *
     * @param \DateTime $fechaEntrega
     *
     * @return Pedido
     */
    public function setFechaEntrega($fechaEntrega)
    {
        $this->fechaEntrega = $fechaEntrega;

        return $this;
    }

    /**
     * Get fechaEntrega
     *
     * @return \DateTime
     */
    public function getFechaEntrega()
    {
        return $this->fechaEntrega;
    }

    /**
     * Set franjaHora
     *
     * @param integer $franjaHora
     *
     * @return Pedido
     */
    public function setFranjaHora($franjaHora)
    {
        $this->franjaHora = $franjaHora;

        return $this;
    }

    /**
     * Get franjaHora
     *
     * @return integer
     */
    public function getFranjaHora()
    {
        return $this->franjaHora;
    }

    /**
     * Set domicilio
     *
     * @param \Backend\ApiRestBundle\Entity\Domicilio $domicilio
     *
     * @return Pedido
     */
    public function setDomicilio(\Backend\ApiRestBundle\Entity\Domicilio $domicilio = null)
    {
        $this->domicilio = $domicilio;

        return $this;
    }

    /**
     * Get domicilio
     *
     * @return \Backend\ApiRestBundle\Entity\Domicilio
     */
    public function getDomicilio()
    {
        return $this->domicilio;
    }

    /**
     * Set hecho
     *
     * @param boolean $hecho
     *
     * @return Pedido
     */
    public function setHecho($hecho)
    {
        $this->hecho = $hecho;

        return $this;
    }

    /**
     * Get hecho
     *
     * @return boolean
     */
    public function getHecho()
    {
        return $this->hecho;
    }

    /**
     * Set driver
     *
     * @param \Backend\ApiRestBundle\Entity\Driver $driver
     *
     * @return Pedido
     */
    public function setDriver(\Backend\ApiRestBundle\Entity\Driver $driver = null)
    {
        $this->driver = $driver;

        return $this;
    }

    /**
     * Get driver
     *
     * @return \Backend\ApiRestBundle\Entity\Driver
     */
    public function getDriver()
    {
        return $this->driver;
    }
}
