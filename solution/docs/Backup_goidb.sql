-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 10, 2018 at 09:59 PM
-- Server version: 5.7.22-0ubuntu0.16.04.1
-- PHP Version: 7.0.30-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `goidb`
--

-- --------------------------------------------------------

--
-- Table structure for table `Cliente`
--

CREATE TABLE `Cliente` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `apellidos` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `Cliente`
--

INSERT INTO `Cliente` (`id`, `nombre`, `apellidos`, `email`) VALUES
(1, 'Andrés', 'Franco Cárdenas', 'andres.afranco@mail.com'),
(8, 'Laura', 'Candamir Gomez', 'laura.gomez@mail.com'),
(9, 'Alejandro', 'Marulanda Moreno', 'alejo.marula@mail.com');

-- --------------------------------------------------------

--
-- Table structure for table `Domicilio`
--

CREATE TABLE `Domicilio` (
  `id` int(11) NOT NULL,
  `cliente` int(11) NOT NULL,
  `direccion` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `Domicilio`
--

INSERT INTO `Domicilio` (`id`, `cliente`, `direccion`) VALUES
(1, 1, 'Madrid.\r\nCalle Castelló 115, 5C\r\nCP 28543 '),
(8, 8, 'Av. Robira 4, Bajo IZ 3'),
(9, 8, 'Av. Noriega 12, Bajo 3'),
(10, 8, 'Av. Constitución 98, 2D'),
(11, 9, 'Barcelona, Barceloneta. Calle Nigeria 4, CP 23457'),
(12, 9, 'Madrid, Fuenlabrada. Calle Carlos III, 23 2A');

-- --------------------------------------------------------

--
-- Table structure for table `Driver`
--

CREATE TABLE `Driver` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `apellidos` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `Driver`
--

INSERT INTO `Driver` (`id`, `nombre`, `apellidos`) VALUES
(1, 'Miguel', 'Rojas'),
(2, 'Angela', 'Guijón Cuartas'),
(3, 'Jose', 'Sanchez Mora'),
(4, 'Ruben', 'Betancourt Rosero');

-- --------------------------------------------------------

--
-- Table structure for table `Pedido`
--

CREATE TABLE `Pedido` (
  `id` int(11) NOT NULL,
  `domicilio` int(11) NOT NULL,
  `fecha_entrega` datetime NOT NULL,
  `franja_hora` int(11) NOT NULL,
  `driver` int(11) DEFAULT NULL,
  `hecho` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `Pedido`
--

INSERT INTO `Pedido` (`id`, `domicilio`, `fecha_entrega`, `franja_hora`, `driver`, `hecho`) VALUES
(1, 1, '2018-06-12 00:00:00', 2, 1, 0),
(2, 8, '2018-06-15 20:31:04', 4, 2, 0),
(3, 9, '2018-06-12 20:37:00', 4, 3, 0),
(4, 10, '2018-06-12 20:56:41', 2, 1, 0),
(5, 11, '2018-06-12 21:13:44', 7, 3, 0),
(6, 12, '2018-06-16 21:17:10', 5, 1, 0),
(7, 8, '2018-06-16 21:18:55', 5, 3, 0),
(8, 8, '2018-06-16 21:35:26', 5, 2, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `Cliente`
--
ALTER TABLE `Cliente`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `Domicilio`
--
ALTER TABLE `Domicilio`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_1965AC72F41C9B25` (`cliente`);

--
-- Indexes for table `Driver`
--
ALTER TABLE `Driver`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `Pedido`
--
ALTER TABLE `Pedido`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_C34013F89B942ED1` (`domicilio`),
  ADD KEY `IDX_C34013F811667CD9` (`driver`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `Cliente`
--
ALTER TABLE `Cliente`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `Domicilio`
--
ALTER TABLE `Domicilio`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `Driver`
--
ALTER TABLE `Driver`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `Pedido`
--
ALTER TABLE `Pedido`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `Domicilio`
--
ALTER TABLE `Domicilio`
  ADD CONSTRAINT `FK_1965AC72F41C9B25` FOREIGN KEY (`cliente`) REFERENCES `Cliente` (`id`);

--
-- Constraints for table `Pedido`
--
ALTER TABLE `Pedido`
  ADD CONSTRAINT `FK_C34013F811667CD9` FOREIGN KEY (`driver`) REFERENCES `Driver` (`id`),
  ADD CONSTRAINT `FK_C34013F89B942ED1` FOREIGN KEY (`domicilio`) REFERENCES `Domicilio` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
