<?php

namespace Backend\ApiRestBundle\Entity;

use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation\Groups;

use Doctrine\ORM\Mapping as ORM;

/**
 * Cliente
 *
 * @ORM\Table(name="Cliente")
 * @ORM\Entity(repositoryClass="Backend\ApiRestBundle\Repository\ClienteRepository")
 * @UniqueEntity(fields="email", message="Email ya existe")
 */
class Cliente
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @Groups({"list_pedido"})
     */
    private $id;

    /**
     * @var string
     * @Assert\NotBlank(message = "Nombre del cliente es requerido")
     *
     * @ORM\Column(name="nombre", type="string", length=255, nullable=false)
     *
     * @Groups({"tareas_pendientes","list_pedido"})
     */
    private $nombre;

    /**
     * @var string
     * @Assert\NotBlank(message = "Apellido(s) requerido(s)")
     *
     * @ORM\Column(name="apellidos", type="string", length=255, nullable=false)
     *
     * @Groups({"tareas_pendientes","list_pedido"})
     */
    private $apellidos;

    /**
     * @var string
     * @Assert\NotBlank(message = "Email es requerido")
     * @Assert\Email(
     *     message = "Email no valido",
     *     checkMX = false)
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=false)
     *
     * @Groups({"tareas_pendientes","list_pedido"})
     */
    private $email;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     *
     * @return Cliente
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set apellidos
     *
     * @param string $apellidos
     *
     * @return Cliente
     */
    public function setApellidos($apellidos)
    {
        $this->apellidos = $apellidos;

        return $this;
    }

    /**
     * Get apellidos
     *
     * @return string
     */
    public function getApellidos()
    {
        return $this->apellidos;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Cliente
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }
}
