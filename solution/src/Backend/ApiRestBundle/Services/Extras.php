<?php

namespace Backend\ApiRestBundle\Services;

use Doctrine\ORM\EntityManager;

/**
 * Servicio para manejar funciones complementarias del API-REST
 * Class Objects
 * @package Backend\ApiRestBundle\Services
 */
class Extras
{
    protected $em;

	public function __construct(EntityManager $em)
	{
		$this->em = $em;
	}

    /**
     * Devuelve un Obj driver aleatorio
     * @return mixed
     */
    public function getRandomObjDriver(){
        $drivers_array = array();
        $drivers = $this->em->getRepository('BackendApiRestBundle:Driver')->findAll();
        foreach ($drivers as $driver) $drivers_array[] = $driver->getId();

        return  $this->em->getRepository('BackendApiRestBundle:Driver')->find(array_rand($drivers_array));

    }


}
?>